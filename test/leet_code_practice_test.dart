import 'package:leet_code_practice/leet_code_practice.dart';
import 'package:test/test.dart';

void main() {
  test('calculate', () {
    expect(calculate(), 42);
  });

  test('twoSum', () {
    expect(twoSum([2, 7, 11, 15], 9), [0, 1]);
  });

  test('reverse', () {
    expect(reverse(-123), -321);
  });

  test('isPalindrome', () {
    expect(isPalindrome(1123211), true);
  });

  test('romanToInt1', () {
    expect(romanToInt('III'), 3);
  });

  test('romanToInt2', () {
    expect(romanToInt('IV'), 4);
  });

  test('romanToInt3', () {
    expect(romanToInt('MCMXCIV'), 1994);
  });

  test('longestCommonPrefix1', () {
    expect(longestCommonPrefix(['flower', 'flow', 'flight']), 'fl');
  });

  test('longestCommonPrefix2', () {
    expect(longestCommonPrefix(['dog', 'racecar', 'car']), '');
  });

  test('isParenthesesValid1', () {
    expect(isParenthesesValid('()'), true);
  });

  test('isParenthesesValid2', () {
    expect(isParenthesesValid('()[]{}'), true);
  });

  test('isParenthesesValid3', () {
    expect(isParenthesesValid('(]'), false);
  });

  test('isParenthesesValid4', () {
    expect(isParenthesesValid('([)]'), false);
  });

  test('isParenthesesValid5', () {
    expect(isParenthesesValid('{[]}'), true);
  });

  test('mergeTwoLists', () {
    expect(mergeTwoLists([1, 2, 4], [1, 3, 4]), [1, 1, 2, 3, 4, 4]);
  });

  test('removeDuplicates1', () {
    expect(removeDuplicates([1, 1, 2]), 2);
  });

  test('removeDuplicates2', () {
    expect(removeDuplicates([0, 0, 1, 1, 1, 2, 2, 3, 3, 4]), 5);
  });

  test('removeElement1', () {
    expect(removeElement([3, 2, 2, 3], 3), 2);
  });

  test('removeElement2', () {
    expect(removeElement([0, 1, 2, 2, 3, 0, 4, 2], 2), 5);
  });

  test('strStr1', () {
    expect(strStr('hello', 'll'), 2);
  });

  test('strStr2', () {
    expect(strStr('', ''), 0);
  });

  test('searchInsert1', () {
    expect(searchInsert([1, 3, 5, 6], 5), 2);
  });

  test('searchInsert2', () {
    expect(searchInsert([1, 3, 5, 6], 2), 1);
  });

  test('searchInsert3', () {
    expect(searchInsert([1, 3, 5, 6], 7), 4);
  });

  test('maxSubArray1', () {
    expect(maxSubArray([-2, 1, -3, 4, -1, 2, 1, -5, 4]), 6);
  });

  test('maxSubArray2', () {
    expect(maxSubArray([1]), 1);
  });

  test('maxSubArray3', () {
    expect(maxSubArray([5, 4, -1, 7, 8]), 23);
  });

  test('lengthOfLastWord1', () {
    expect(lengthOfLastWord('Hello World'), 5);
  });

  test('lengthOfLastWord2', () {
    expect(lengthOfLastWord(' '), 0);
  });
}
