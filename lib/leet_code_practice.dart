import 'package:tuple/tuple.dart';
import 'dart:collection';

int calculate() {
  return 6 * 7;
}

//--

// (1)
// Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
// You may assume that each input would have exactly one solution, and you may not use the same element twice.
// You can return the answer in any order.
// Example 1:
// Input: nums = [2,7,11,15], target = 9
// Output: [0,1]
// Output: Because nums[0] + nums[1] == 9, we return [0, 1].
// Example 2:
// Input: nums = [3,2,4], target = 6
// Output: [1,2]
// Example 3:
// Input: nums = [3,3], target = 6
// Output: [0,1]
List<int> twoSum(List<int> nums, int target) {
  for (int i = 0; i < nums.length; i++) {
    for (int j = 0; j < nums.length; j++) {
      if (nums[i] + nums[j] == target) {
        return [i, j];
      }
    }
  }
  return [];
}

// (7)
// Given a signed 32-bit integer x, return x with its digits reversed.
// If reversing x causes the value to go outside the signed 32-bit integer range [-231, 231 - 1], then return 0.
// Assume the environment does not allow you to store 64-bit integers (signed or unsigned).
// Example 1:
// Input: x = 123
// Output: 321
// Example 2:
// Input: x = -123
// Output: -321
// Example 3:
// Input: x = 120
// Output: 21
// Example 4:
// Input: x = 0
// Output: 0
int reverse(int x) {
  int result = 0;
  //Is x a negative number?
  bool xIsNegative = x < 0 ? true : false;
  int absX = x.abs();
  while (absX != 0) {
    int remainder = absX % 10;
    result = result * 10 + remainder;
    absX = absX ~/ 10;
  }

  if (xIsNegative) {
    result = -result;
  }
  return result;
}

// (9)
// Given an integer x, return true if x is palindrome integer.
// An integer is a palindrome when it reads the same backward as forward. For example, 121 is palindrome while 123 is not.
// Example 1:
// Input: x = 121
// Output: true
// Example 2:
// Input: x = -121
// Output: false
// Explanation: From left to right, it reads -121. From right to left, it becomes 121-. Therefore it is not a palindrome.
// Example 3:
// Input: x = 10
// Output: false
// Explanation: Reads 01 from right to left. Therefore it is not a palindrome.
// Example 4:
// Input: x = -101
// Output: false
bool isPalindrome(int x) {
  if (x < 0) {
    return false;
  }
  String xStr = x.toString();
  for (int i = 0; i < xStr.length; ++i) {
    //Just compare the first xStr.length / 2 to the last xStr.length / 2 digits...
    if (xStr[i] != xStr[xStr.length - 1 - i]) {
      return false;
    }

    if (i > xStr.length / 2) {
      break;
    }
  }
  return true;
}

// (13)
// Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.
// Symbol       Value
// I             1
// V             5
// X             10
// L             50
// C             100
// D             500
// M             1000
// For example, 2 is written as II in Roman numeral, just two one's added together. 12 is written as XII, which is simply X + II. The number 27 is written as XXVII, which is XX + V + II.
// Roman numerals are usually written largest to smallest from left to right.
// However, the numeral for four is not IIII. Instead, the number four is written as IV.
// Because the one is before the five we subtract it making four. The same principle applies to the number nine, which is written as IX.
// There are six instances where subtraction is used:
// I can be placed before V (5) and X (10) to make 4 and 9.
// X can be placed before L (50) and C (100) to make 40 and 90.
// C can be placed before D (500) and M (1000) to make 400 and 900.
// Given a roman numeral, convert it to an integer.
// Example 1:
// Input: s = "III"
// Output: 3
// Example 2:
// Input: s = "IV"
// Output: 4
// Example 3:
// Input: s = "IX"
// Output: 9
// Example 4:
// Input: s = "LVIII"
// Output: 58
// Explanation: L = 50, V= 5, III = 3.
// Example 5:
// Input: s = "MCMXCIV"
// Output: 1994
// Explanation: M = 1000, CM = 900, XC = 90 and IV = 4.
int romanToInt(String s) {
  List<Tuple2<String, int>> symbolValueList = [
    Tuple2<String, int>('I', 1),
    Tuple2<String, int>('IV', 4),
    Tuple2<String, int>('V', 5),
    Tuple2<String, int>('IX', 9),
    Tuple2<String, int>('X', 10),
    Tuple2<String, int>('XL', 40),
    Tuple2<String, int>('L', 50),
    Tuple2<String, int>('XC', 90),
    Tuple2<String, int>('C', 100),
    Tuple2<String, int>('CD', 400),
    Tuple2<String, int>('D', 500),
    Tuple2<String, int>('CM', 900),
    Tuple2<String, int>('M', 1000)
  ];

  int resultValue = 0;
  for (int i = symbolValueList.length - 1; i >= 0; i--) {
    while (s.startsWith(symbolValueList[i].item1)) {
      s = s.substring(symbolValueList[i].item1.length);
      resultValue += symbolValueList[i].item2;
    }
  }

  return resultValue;
}

// (14)
// Write a function to find the longest common prefix string amongst an array of strings.
// If there is no common prefix, return an empty string "".

// Example 1:
// Input: strs = ["flower","flow","flight"]
// Output: "fl"

// Example 2:
// Input: strs = ["dog","racecar","car"]
// Output: ""
// Explanation: There is no common prefix among the input strings.
String longestCommonPrefix(List<String> strs) {
  String resPrefix = '';
  if (strs.isNotEmpty) {
    //Assume the first String as the prefix.
    resPrefix = strs[0];

    for (int i = 0; i < strs.length; i++) {
      for (int j = 0; j < strs.length; j++) {
        if (i == j) {
          continue;
        }

        String foundPrefix = '';
        int compareLength = (strs[i].length < strs[j].length)
            ? (strs[i].length)
            : (strs[j].length);
        for (int l = 0; l < compareLength; l++) {
          if (strs[i][l] == strs[j][l]) {
            foundPrefix += strs[i][l];
          } else {
            break;
          }
        }

        //Is the foundPrefix shorter than resPrefix?
        if (foundPrefix.length < resPrefix.length) {
          resPrefix = foundPrefix;
        }
      }
    }
  }

  return resPrefix;
}

// (20)
// Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.
// An input string is valid if:
// Open brackets must be closed by the same type of brackets.
// Open brackets must be closed in the correct order.

// Example 1:
// Input: s = "()"
// Output: true

// Example 2:
// Input: s = "()[]{}"
// Output: true

// Example 3:
// Input: s = "(]"
// Output: false

// Example 4:
// Input: s = "([)]"
// Output: false

// Example 5:
// Input: s = "{[]}"
// Output: true
bool isParenthesesValid(String s) {
  List<String> parenthesesSets = ['()', '[]', '{}'];

  while (parenthesesSets.any((element) => s.contains(element))) {
    parenthesesSets.forEach((element) {
      s = s.replaceAll(element, '');
    });
  }

  if (s.isNotEmpty) {
    return false;
  }

  return true;
}

// (21)
// Merge two sorted linked lists and return it as a sorted list.
// The list should be made by splicing together the nodes of the first two lists.

// Example 1:
// Input: l1 = [1,2,4], l2 = [1,3,4]
// Output: [1,1,2,3,4,4]
List<int> mergeTwoLists(List<int> l1, List<int> l2) {
  List<int> resList = [];
  while (l1.isNotEmpty || l2.isNotEmpty) {
    if (l1.isEmpty) {
      resList.add(l2.first);
      l2.removeAt(0);
    } else if (l2.isEmpty) {
      resList.add(l1.first);
      l1.removeAt(0);
    } else {
      //Compare the first element of the two list.
      if (l1.first <= l2.first) {
        //Choose l1's first element,
        resList.add(l1.first);
        l1.removeAt(0);
      } else {
        //Choose l2's first element,
        resList.add(l2.first);
        l2.removeAt(0);
      }
    }
  }
  return resList;
}

// (26)
// Given a sorted array nums, remove the duplicates in-place such that each element appears only once and returns the new length.
// Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.

// Clarification:
// Confused why the returned value is an integer but your answer is an array?

// Note that the input array is passed in by reference, which means a modification to the input array will be known to the caller as well.
// Internally you can think of this:

// // nums is passed in by reference. (i.e., without making a copy)
// int len = removeDuplicates(nums);
// // any modification to nums in your function would be known by the caller.
// // using the length returned by your function, it prints the first len elements.
// for (int i = 0; i < len; i++) {
//     print(nums[i]);
// }

// Example 1:
// Input: nums = [1,1,2]
// Output: 2, nums = [1,2]
// Explanation: Your function should return length = 2, with the first two elements of nums being 1 and 2 respectively.
// It doesn't matter what you leave beyond the returned length.

// Example 2:
// Input: nums = [0,0,1,1,1,2,2,3,3,4]
// Output: 5, nums = [0,1,2,3,4]
// Explanation: Your function should return length = 5, with the first five elements of nums being modified to 0, 1, 2, 3, and 4 respectively.
// It doesn't matter what values are set beyond the returned length.
int removeDuplicates(List<int> nums) {
  int confirmedIndex = -1;
  int currentFoundGreatestNum = -1;
  for (int i = 0; i < nums.length; i++) {
    if (currentFoundGreatestNum < nums[i]) {
      currentFoundGreatestNum = nums[i];
      confirmedIndex += 1;
      if (confirmedIndex != i) {
        //Swap the number in confirmedIndex with the number in i.
        int temp = nums[confirmedIndex];
        nums[confirmedIndex] = nums[i];
        nums[i] = temp;
      }
    }
  }

  return confirmedIndex + 1;
}

// (27)
// Given an array nums and a value val, remove all instances of that value in-place and return the new length.
// Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.
// The order of elements can be changed. It doesn't matter what you leave beyond the new length.
// Clarification:
// Confused why the returned value is an integer but your answer is an array?
// Note that the input array is passed in by reference, which means a modification to the input array will be known to the caller as well.
// Internally you can think of this:
// // nums is passed in by reference. (i.e., without making a copy)
// int len = removeElement(nums, val);
// // any modification to nums in your function would be known by the caller.
// // using the length returned by your function, it prints the first len elements.
// for (int i = 0; i < len; i++) {
//     print(nums[i]);
// }

// Example 1:
// Input: nums = [3,2,2,3], val = 3
// Output: 2, nums = [2,2]
// Explanation: Your function should return length = 2, with the first two elements of nums being 2.
// It doesn't matter what you leave beyond the returned length. For example if you return 2 with nums = [2,2,3,3] or nums = [2,2,0,0], your answer will be accepted.

// Example 2:
// Input: nums = [0,1,2,2,3,0,4,2], val = 2
// Output: 5, nums = [0,1,4,0,3]
// Explanation: Your function should return length = 5, with the first five elements of nums containing 0, 1, 3, 0, and 4. Note that the order of those five elements can be arbitrary.
// It doesn't matter what values are set beyond the returned length.
int removeElement(List<int> nums, int val) {
  int confirmedIndex = nums.length;

  for (int i = nums.length - 1; i >= 0; i--) {
    if (nums[i] == val) {
      //Found one!
      confirmedIndex -= 1;
      if (confirmedIndex != i) {
        //Do swap.
        int temp = nums[confirmedIndex];
        nums[confirmedIndex] = nums[i];
        nums[i] = temp;
      }
    }
  }

  return confirmedIndex;
}

// (28)
// Implement strStr().
// Return the index of the first occurrence of needle in haystack, or -1 if needle is not part of haystack.

// Clarification:
// What should we return when needle is an empty string? This is a great question to ask during an interview.
// For the purpose of this problem, we will return 0 when needle is an empty string. This is consistent to C's strstr() and Java's indexOf().

// Example 1:
// Input: haystack = "hello", needle = "ll"
// Output: 2

// Example 2:
// Input: haystack = "aaaaa", needle = "bba"
// Output: -1

// Example 3:
// Input: haystack = "", needle = ""
// Output: 0
int strStr(String haystack, String needle) {
  if (needle.isEmpty) {
    return 0;
  }

  int res = -1;
  for (int i = 0; i < haystack.length; i++) {
    //Compare start from index i to i + needle.length
    bool allMatch = true;
    for (int j = 0; j < needle.length; j++) {
      if (needle[j] != haystack[i + j]) {
        allMatch = false;
        break;
      }
    }
    if (allMatch) {
      res = i;
      break;
    }
  }
  return res;
}

// (35)
// Given a sorted array of distinct integers and a target value, return the index if the target is found.
// If not, return the index where it would be if it were inserted in order.
// You must write an algorithm with O(log n) runtime complexity.
int searchInsert(List<int> nums, int target) {
  //[Binary search]
  int startIndex = 0;
  int endIndex = nums.length - 1;
  //To be the legal insert index for the target
  int insertIndex = 0;

  while (startIndex <= endIndex) {
    int midIndex = startIndex + (endIndex - startIndex) ~/ 2;

    if (nums[midIndex] == target) {
      return midIndex;
    } else if (nums[midIndex] > target) {
      endIndex = midIndex - 1;
      insertIndex = midIndex;
    } else {
      startIndex = midIndex + 1;
      insertIndex = midIndex + 1;
    }
  }

  return insertIndex;
}

// (53)
// Given an integer array nums,
// find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.

// Example 1:
// Input: nums = [-2,1,-3,4,-1,2,1,-5,4]
// Output: 6
// Explanation: [4,-1,2,1] has the largest sum = 6.

// Example 2:
// Input: nums = [1]
// Output: 1

// Example 3:
// Input: nums = [5,4,-1,7,8]
// Output: 23
int maxSubArray(List<int> nums) {
  if (nums.isNotEmpty) {
    int currentMax = nums[0];
    int currentSum = 0;
    for (int i = 0; i < nums.length; ++i) {
      //This will get rid of any negative prefix.
      if (currentSum < 0) {
        currentSum = 0;
      }

      currentSum += nums[i];
      if (currentSum > currentMax) {
        currentMax = currentSum;
      }
    }
    return currentMax;
  }

  return -1;
}

// (58)
// Given a string s consists of some words separated by spaces, return the length of the last word in the string.
// If the last word does not exist, return 0.
// A word is a maximal substring consisting of non-space characters only.

// Example 1:
// Input: s = "Hello World"
// Output: 5

// Example 2:
// Input: s = " "
// Output: 0
int lengthOfLastWord(String s) {
  int currentLength = 0;
  for (int i = 0; i < s.length; ++i) {
    if (s[i] == ' ') {
      currentLength = 0;
    } else {
      currentLength += 1;
    }
  }
  return currentLength;
}
